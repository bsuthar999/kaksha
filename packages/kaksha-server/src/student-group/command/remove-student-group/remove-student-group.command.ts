import { ICommand } from '@nestjs/cqrs';

export class RemoveStudentGroupCommand implements ICommand {
  constructor(public readonly uuid: string) {}
}
