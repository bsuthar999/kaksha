import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { StudentGroupService } from '../../entity/student-group/student-group.service';
import { StudentGroupRemovedEvent } from './student-group-removed.event';

@EventsHandler(StudentGroupRemovedEvent)
export class StudentGroupRemovedCommandHandler
  implements IEventHandler<StudentGroupRemovedEvent> {
  constructor(private readonly studentGroupService: StudentGroupService) {}
  async handle(event: StudentGroupRemovedEvent) {
    const { studentGroup } = event;
    await this.studentGroupService.deleteOne({ uuid: studentGroup.uuid });
  }
}
