import {
  Controller,
  Post,
  UseGuards,
  UsePipes,
  Body,
  ValidationPipe,
  Req,
  Param,
  Get,
  Query,
} from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { TokenGuard } from '../../../auth/guards/token.guard';
import { DepartmentDto } from '../../entity/department/department-dto';
import { AddDepartmentCommand } from '../../command/add-department/add-department.command';
import { RemoveDepartmentCommand } from '../../command/remove-department/remove-department.command';
import { UpdateDepartmentCommand } from '../../command/update-department/update-department.command';
import { RetrieveDepartmentQuery } from '../../query/get-department/retrieve-department.query';
import { RetrieveDepartmentListQuery } from '../../query/list-department/retrieve-department-list.query';
import { UpdateDepartmentDto } from '../../entity/department/update-department-dto';

@Controller('department')
export class DepartmentController {
  constructor(
    private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus,
  ) {}

  @Post('v1/create')
  @UseGuards(TokenGuard)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  create(@Body() departmentPayload: DepartmentDto, @Req() req) {
    return this.commandBus.execute(
      new AddDepartmentCommand(departmentPayload, req),
    );
  }

  @Post('v1/remove/:uuid')
  @UseGuards(TokenGuard)
  remove(@Param('uuid') uuid: string) {
    return this.commandBus.execute(new RemoveDepartmentCommand(uuid));
  }

  @Get('v1/get/:uuid')
  @UseGuards(TokenGuard)
  async getClient(@Param('uuid') uuid: string, @Req() req) {
    return await this.queryBus.execute(new RetrieveDepartmentQuery(uuid, req));
  }

  @Get('v1/list')
  @UseGuards(TokenGuard)
  getClientList(
    @Query('offset') offset = 0,
    @Query('limit') limit = 10,
    @Query('search') search = '',
    @Query('sort') sort,
    @Req() clientHttpRequest,
  ) {
    if (sort !== 'ASC') {
      sort = 'DESC';
    }
    return this.queryBus.execute(
      new RetrieveDepartmentListQuery(
        offset,
        limit,
        sort,
        search,
        clientHttpRequest,
      ),
    );
  }

  @Post('v1/update')
  @UseGuards(TokenGuard)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  updateClient(@Body() updatePayload: UpdateDepartmentDto) {
    return this.commandBus.execute(new UpdateDepartmentCommand(updatePayload));
  }
}
