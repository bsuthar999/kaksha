import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { DepartmentAddedEvent } from './department-added.event';
import { DepartmentService } from '../../entity/department/department.service';

@EventsHandler(DepartmentAddedEvent)
export class DepartmentAddedCommandHandler
  implements IEventHandler<DepartmentAddedEvent> {
  constructor(private readonly departmentService: DepartmentService) {}
  async handle(event: DepartmentAddedEvent) {
    const { department } = event;
    await this.departmentService.create(department);
  }
}
