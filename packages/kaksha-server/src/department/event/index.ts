import { DepartmentAddedCommandHandler } from './department-added/department-added.handler';
import { DepartmentRemovedCommandHandler } from './department-removed/department-removed.handler';
import { DepartmentUpdatedCommandHandler } from './department-updated/department-updated.handler';

export const DepartmentEventManager = [
  DepartmentAddedCommandHandler,
  DepartmentRemovedCommandHandler,
  DepartmentUpdatedCommandHandler,
];
