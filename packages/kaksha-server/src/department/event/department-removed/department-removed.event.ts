import { IEvent } from '@nestjs/cqrs';
import { Department } from '../../entity/department/department.entity';

export class DepartmentRemovedEvent implements IEvent {
  constructor(public department: Department) {}
}
