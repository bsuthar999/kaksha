import { VideoAddedCommandHandler } from './video-added/video-added.handler';
import { VideoRemovedCommandHandler } from './video-removed/video-removed.handler';
import { VideoUpdatedCommandHandler } from './video-updated/video-updated.handler';

export const VideoEventManager = [
  VideoAddedCommandHandler,
  VideoRemovedCommandHandler,
  VideoUpdatedCommandHandler,
];
