import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { VideoService } from '../../entity/video/video.service';
import { VideoRemovedEvent } from './video-removed.event';

@EventsHandler(VideoRemovedEvent)
export class VideoRemovedCommandHandler
  implements IEventHandler<VideoRemovedEvent> {
  constructor(private readonly videoService: VideoService) {}
  async handle(event: VideoRemovedEvent) {
    const { video } = event;
    await this.videoService.deleteOne({ uuid: video.uuid });
  }
}
