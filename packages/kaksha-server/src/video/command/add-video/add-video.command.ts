import { ICommand } from '@nestjs/cqrs';
import { VideoDto } from '../../entity/video/video-dto';

export class AddVideoCommand implements ICommand {
  constructor(
    public videoPayload: VideoDto,
    public readonly clientHttpRequest: any,
  ) {}
}
