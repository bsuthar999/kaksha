import { Column, ObjectIdColumn, BaseEntity, ObjectID, Entity } from 'typeorm';

@Entity()
export class Video extends BaseEntity {
  @ObjectIdColumn()
  _id: ObjectID;

  @Column()
  uuid: string;

  @Column()
  isSynced: boolean;

  @Column()
  name: string;

  @Column()
  docstatus: number;

  @Column()
  title: string;

  @Column()
  provider: string;

  @Column()
  url: string;

  @Column()
  publish_date: string;

  @Column()
  duration: string;

  @Column()
  description: string;

  @Column()
  doctype: string;
}
