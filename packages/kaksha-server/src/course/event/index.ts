import { CourseAddedEventHandler } from './course-added/course-added.handler';
import { CourseRemovedEventHandler } from './course-removed/course.removed.handler';
import { CourseUpdatedEventHandler } from './course-updated/course-updated.handler';

export const CourseEventManager = [
  CourseAddedEventHandler,
  CourseRemovedEventHandler,
  CourseUpdatedEventHandler,
];
