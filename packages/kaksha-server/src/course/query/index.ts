import { RetrieveCourseQueryHandler } from './get-course/retrieve-course.handler';
import { RetrieveCourseListQueryHandler } from './list-course/retrieve-course-list.handler';

export const CourseQueryManager = [
  RetrieveCourseQueryHandler,
  RetrieveCourseListQueryHandler,
];
