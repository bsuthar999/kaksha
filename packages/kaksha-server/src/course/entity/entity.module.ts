import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Course } from './course/course.entity';
import { CourseService } from './course/course.service';
import { CqrsModule } from '@nestjs/cqrs';

@Module({
  imports: [TypeOrmModule.forFeature([Course]), CqrsModule],
  providers: [CourseService],
  exports: [CourseService],
})
export class CourseEntitiesModule {}
