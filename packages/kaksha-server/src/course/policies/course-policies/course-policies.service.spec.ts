import { Test, TestingModule } from '@nestjs/testing';
import { CoursePoliciesService } from './course-policies.service';

describe('CoursePoliciesService', () => {
  let service: CoursePoliciesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CoursePoliciesService],
    }).compile();

    service = module.get<CoursePoliciesService>(CoursePoliciesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
