import {
  IsString,
  IsOptional,
  ValidateNested,
  IsNumber,
} from 'class-validator';
import { Type } from 'class-transformer';

export class TeacherDto {
  @IsString()
  @IsOptional()
  name: string;

  @IsNumber()
  @IsOptional()
  docstatus: number;

  @IsString()
  @IsOptional()
  instructor_name: string;

  @IsString()
  @IsOptional()
  employee: string;

  @IsString()
  @IsOptional()
  doctype: string;

  @ValidateNested()
  @Type(() => TeacherLogDto)
  instructor_log: TeacherLogDto[];
}
export class TeacherLogDto {
  @IsString()
  @IsOptional()
  name: string;

  @IsNumber()
  @IsOptional()
  docstatus: number;

  @IsString()
  @IsOptional()
  academic_year: string;

  @IsString()
  @IsOptional()
  academic_term: string;

  @IsString()
  @IsOptional()
  program: string;

  @IsString()
  @IsOptional()
  course: string;

  @IsString()
  @IsOptional()
  doctype: string;
}
