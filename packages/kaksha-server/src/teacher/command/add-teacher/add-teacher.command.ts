import { ICommand } from '@nestjs/cqrs';
import { TeacherDto } from '../../entity/teacher/teacher-dto';
import { ClientHttpRequestTokenInterface } from '../../../common/client-request-token.interace';

export class AddTeacherCommand implements ICommand {
  constructor(
    public teacherPayload: TeacherDto,
    public readonly clientHttpRequest: ClientHttpRequestTokenInterface,
  ) {}
}
