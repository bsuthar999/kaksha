import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TokenCache } from './token-cache/token-cache.entity';
import { TokenCacheService } from './token-cache/token-cache.service';
import { TOKEN_CACHE_CONNECTION } from '../../constants/typeorm.connection';

@Module({
  imports: [TypeOrmModule.forFeature([TokenCache], TOKEN_CACHE_CONNECTION)],
  providers: [TokenCacheService],
  exports: [TokenCacheService],
})
export class AuthEntitiesModule {}
