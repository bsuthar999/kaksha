import { IEvent } from '@nestjs/cqrs';
import { CourseSchedule } from '../../entity/course-schedule/course-schedule.entity';

export class CourseScheduleRemovedEvent implements IEvent {
  constructor(public courseSchedule: CourseSchedule) {}
}
