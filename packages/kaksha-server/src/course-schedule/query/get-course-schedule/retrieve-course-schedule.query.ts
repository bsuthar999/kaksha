import { IQuery } from '@nestjs/cqrs';

export class RetrieveCourseScheduleQuery implements IQuery {
  constructor(public readonly uuid: string, public readonly req: any) {}
}
