import { ICommand } from '@nestjs/cqrs';

export class RemoveCourseScheduleCommand implements ICommand {
  constructor(public readonly uuid: string) {}
}
