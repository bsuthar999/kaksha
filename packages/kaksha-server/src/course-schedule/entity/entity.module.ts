import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CourseSchedule } from './course-schedule/course-schedule.entity';
import { CourseScheduleService } from './course-schedule/course-schedule.service';
import { CqrsModule } from '@nestjs/cqrs';

@Module({
  imports: [TypeOrmModule.forFeature([CourseSchedule]), CqrsModule],
  providers: [CourseScheduleService],
  exports: [CourseScheduleService],
})
export class CourseScheduleEntitiesModule {}
