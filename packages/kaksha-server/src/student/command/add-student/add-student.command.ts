import { ICommand } from '@nestjs/cqrs';
import { StudentDto } from '../../entity/student/student-dto';
import { ClientHttpRequestTokenInterface } from '../../../common/client-request-token.interace';

export class AddStudentCommand implements ICommand {
  constructor(
    public studentPayload: StudentDto,
    public readonly clientHttpRequest: ClientHttpRequestTokenInterface,
  ) {}
}
