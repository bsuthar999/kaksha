import { IEvent } from '@nestjs/cqrs';
import { Student } from '../../entity/student/student.entity';

export class StudentRemovedEvent implements IEvent {
  constructor(public student: Student) {}
}
