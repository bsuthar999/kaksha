import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { RetrieveQuestionQuery } from './retrieve-question.query';
import { QuestionAggregateService } from '../../aggregates/question-aggregate/question-aggregate.service';

@QueryHandler(RetrieveQuestionQuery)
export class RetrieveQuestionQueryHandler
  implements IQueryHandler<RetrieveQuestionQuery> {
  constructor(private readonly manager: QuestionAggregateService) {}

  async execute(query: RetrieveQuestionQuery) {
    const { req, uuid } = query;
    return this.manager.retrieveQuestion(uuid, req);
  }
}
