import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { RemoveQuestionCommand } from './remove-question.command';
import { QuestionAggregateService } from '../../aggregates/question-aggregate/question-aggregate.service';

@CommandHandler(RemoveQuestionCommand)
export class RemoveQuestionCommandHandler
  implements ICommandHandler<RemoveQuestionCommand> {
  constructor(
    private readonly publisher: EventPublisher,
    private readonly manager: QuestionAggregateService,
  ) {}
  async execute(command: RemoveQuestionCommand) {
    const { removeQuestionPayload, clientHttpRequest } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await aggregate
      .removeQuestion(removeQuestionPayload, clientHttpRequest)
      .toPromise();
    aggregate.commit();
  }
}
