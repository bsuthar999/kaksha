import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { TopicUpdatedEvent } from './topic-updated.event';
import { TopicService } from '../../entity/topic/topic.service';

@EventsHandler(TopicUpdatedEvent)
export class TopicUpdatedEventHandler
  implements IEventHandler<TopicUpdatedEvent> {
  constructor(private readonly object: TopicService) {}

  async handle(event: TopicUpdatedEvent) {
    const { updatePayload } = event;
    await this.object.updateOne(
      { name: updatePayload.name },
      { $set: updatePayload },
    );
  }
}
