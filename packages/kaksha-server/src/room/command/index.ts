import { AddRoomCommandHandler } from './add-room/add-room.handler';
import { RemoveRoomCommandHandler } from './remove-room/remove-room.handler';
import { UpdateRoomCommandHandler } from './update-room/update-room.handler';

export const RoomCommandManager = [
  AddRoomCommandHandler,
  RemoveRoomCommandHandler,
  UpdateRoomCommandHandler,
];
