import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { AddArticleCommand } from './add-article.command';
import { ArticleAggregateService } from '../../aggregates/article-aggregate/article-aggregate.service';

@CommandHandler(AddArticleCommand)
export class AddArticleCommandHandler
  implements ICommandHandler<AddArticleCommand> {
  constructor(
    private publisher: EventPublisher,
    private manager: ArticleAggregateService,
  ) {}
  async execute(command: AddArticleCommand) {
    const { articlePayload, clientHttpRequest } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await aggregate.addArticle(articlePayload, clientHttpRequest);
    aggregate.commit();
  }
}
