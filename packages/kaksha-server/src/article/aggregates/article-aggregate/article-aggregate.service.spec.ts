import { Test, TestingModule } from '@nestjs/testing';
import { ArticleAggregateService } from './article-aggregate.service';
import { ArticleService } from '../../entity/article/article.service';
import { TeacherService } from '../../../teacher/entity/teacher/teacher.service';
import { SettingsService } from '../../../system-settings/aggregates/settings/settings.service';
import { HttpService } from '@nestjs/common';
import { CourseService } from '../../../course/entity/course/course.service';
import { TopicService } from '../../../topic/entity/topic/topic.service';

describe('ArticleAggregateService', () => {
  let service: ArticleAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ArticleAggregateService,
        {
          provide: ArticleService,
          useValue: {},
        },
        {
          provide: TeacherService,
          useValue: {},
        },
        {
          provide: CourseService,
          useValue: {},
        },
        {
          provide: TopicService,
          useValue: {},
        },
        {
          provide: SettingsService,
          useValue: {},
        },
        {
          provide: HttpService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<ArticleAggregateService>(ArticleAggregateService);
  });
  ArticleAggregateService;
  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
