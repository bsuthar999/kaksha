import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { ArticleUpdatedEvent } from './article-updated.event';
import { ArticleService } from '../../entity/article/article.service';

@EventsHandler(ArticleUpdatedEvent)
export class ArticleUpdatedCommandHandler
  implements IEventHandler<ArticleUpdatedEvent> {
  constructor(private readonly object: ArticleService) {}

  async handle(event: ArticleUpdatedEvent) {
    const { updatePayload } = event;
    await this.object.updateOne(
      { uuid: updatePayload.uuid },
      { $set: updatePayload },
    );
  }
}
