import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { ArticleService } from '../../entity/article/article.service';
import { ArticleRemovedEvent } from './article-removed.event';

@EventsHandler(ArticleRemovedEvent)
export class ArticleRemovedCommandHandler
  implements IEventHandler<ArticleRemovedEvent> {
  constructor(private readonly articleService: ArticleService) {}
  async handle(event: ArticleRemovedEvent) {
    const { article } = event;
    await this.articleService.deleteOne({ uuid: article.uuid });
  }
}
