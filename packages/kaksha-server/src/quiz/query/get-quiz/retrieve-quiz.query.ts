import { IQuery } from '@nestjs/cqrs';
import { UpdateTopicStatusDto } from '../../../topic/entity/topic/update-topic-status-dto';

export class RetrieveQuizQuery implements IQuery {
  constructor(
    public readonly offset,
    public readonly limit,
    public readonly sort,
    public readonly search,
    public readonly getQuizPayload: UpdateTopicStatusDto,
    public readonly req: any,
  ) {}
}
