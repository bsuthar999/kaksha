import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { UpdateAttendanceCommand } from './update-attendance.command';
import { AttendanceAggregateService } from '../../aggregates/attendance-aggregate/attendance-aggregate.service';

@CommandHandler(UpdateAttendanceCommand)
export class UpdateAttendanceCommandHandler
  implements ICommandHandler<UpdateAttendanceCommand> {
  constructor(
    private publisher: EventPublisher,
    private manager: AttendanceAggregateService,
  ) {}

  async execute(command: UpdateAttendanceCommand) {
    const { updatePayload } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await aggregate.update(updatePayload);
    aggregate.commit();
  }
}
