import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { AddAttendanceCommand } from './add-attendance.command';
import { AttendanceAggregateService } from '../../aggregates/attendance-aggregate/attendance-aggregate.service';

@CommandHandler(AddAttendanceCommand)
export class AddAttendanceCommandHandler
  implements ICommandHandler<AddAttendanceCommand> {
  constructor(
    private publisher: EventPublisher,
    private manager: AttendanceAggregateService,
  ) {}
  async execute(command: AddAttendanceCommand) {
    const { attendancePayload, clientHttpRequest } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await aggregate.addAttendance(attendancePayload, clientHttpRequest);
    aggregate.commit();
  }
}
