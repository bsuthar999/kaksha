import { Test, TestingModule } from '@nestjs/testing';
import { AttendancePoliciesService } from './attendance-policies.service';

describe('AttendancePoliciesService', () => {
  let service: AttendancePoliciesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [AttendancePoliciesService],
    }).compile();

    service = module.get<AttendancePoliciesService>(AttendancePoliciesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
