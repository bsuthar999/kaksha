import { RetrieveProgramQueryHandler } from './get-program/retrieve-program.handler';
import { RetrieveProgramListQueryHandler } from './list-program/retrieve-program-list.handler';

export const ProgramQueryManager = [
  RetrieveProgramQueryHandler,
  RetrieveProgramListQueryHandler,
];
