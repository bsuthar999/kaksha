import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { ProgramAddedEvent } from './program-added.event';
import { ProgramService } from '../../entity/program/program.service';

@EventsHandler(ProgramAddedEvent)
export class ProgramAddedCommandHandler
  implements IEventHandler<ProgramAddedEvent> {
  constructor(private readonly programService: ProgramService) {}
  async handle(event: ProgramAddedEvent) {
    const { program } = event;
    await this.programService.create(program);
  }
}
