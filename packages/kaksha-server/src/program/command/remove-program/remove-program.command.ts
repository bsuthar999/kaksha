import { ICommand } from '@nestjs/cqrs';

export class RemoveProgramCommand implements ICommand {
  constructor(public readonly uuid: string) {}
}
