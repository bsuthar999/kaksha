import { ICommand } from '@nestjs/cqrs';
import { UpdateProgramDto } from '../../entity/program/update-program-dto';

export class UpdateProgramCommand implements ICommand {
  constructor(public readonly updatePayload: UpdateProgramDto) {}
}
