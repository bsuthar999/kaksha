import { Module, HttpModule } from '@nestjs/common';
import { ProgramAggregatesManager } from './aggregates';
import { ProgramEntitiesModule } from './entity/entity.module';
import { ProgramQueryManager } from './query';
import { CqrsModule } from '@nestjs/cqrs';
import { ProgramCommandManager } from './command';
import { ProgramEventManager } from './event';
import { ProgramController } from './controllers/program/program.controller';
import { ProgramPoliciesService } from './policies/program-policies/program-policies.service';

@Module({
  imports: [ProgramEntitiesModule, CqrsModule, HttpModule],
  controllers: [ProgramController],
  providers: [
    ...ProgramAggregatesManager,
    ...ProgramQueryManager,
    ...ProgramEventManager,
    ...ProgramCommandManager,
    ProgramPoliciesService,
  ],
  exports: [ProgramEntitiesModule],
})
export class ProgramModule {}
