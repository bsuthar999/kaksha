#!/usr/bin/env bash

echo 'Creating application user(s) and db(s)'

mongo token-cache \
        --host localhost \
        --port 27017 \
        -u $MONGODB_PRIMARY_ROOT_USER \
        -p $MONGODB_ROOT_PASSWORD \
        --authenticationDatabase admin \
        --eval "db.createUser({user: 'token-cache', pwd: '$MONGODB_PASSWORD', roles:[{role:'dbOwner', db: 'token-cache'}]});"
