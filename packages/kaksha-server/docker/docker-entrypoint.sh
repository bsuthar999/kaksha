#!/bin/bash

function checkEnv() {
  if [[ -z "$DB_HOST" ]]; then
    echo "DB_HOST is not set"
    exit 1
  fi
  if [[ -z "$DB_NAME" ]]; then
    echo "DB_NAME is not set"
    exit 1
  fi
  if [[ -z "$DB_USER" ]]; then
    echo "DB_USER is not set"
    exit 1
  fi
  if [[ -z "$DB_PASSWORD" ]]; then
    echo "DB_PASSWORD is not set"
    exit 1
  fi
  if [[ -z "$CACHE_DB_NAME" ]]; then
    echo "CACHE_DB_NAME is not set"
    exit 1
  fi
  if [[ -z "$CACHE_DB_PASSWORD" ]]; then
    echo "CACHE_DB_PASSWORD is not set"
    exit 1
  fi
  if [[ -z "$CACHE_DB_USER" ]]; then
    echo "CACHE_DB_USER is not set"
    exit 1
  fi
  if [[ -z "$NODE_ENV" ]]; then
    echo "NODE_ENV is not set"
    exit 1
  fi
}

function checkConnection() {
  # Wait for services
  su craft -c "node ./docker/check-db.js"
}

function configureServer() {
  if [ ! -f .env ]; then
    envsubst '${NODE_ENV}
      ${DB_HOST}
      ${DB_NAME}
      ${DB_USER}
      ${DB_PASSWORD}
      ${CACHE_DB_NAME}
      ${CACHE_DB_PASSWORD}
      ${CACHE_DB_USER}' \
      < docker/env.tmpl > .env
  fi
}

export -f configureServer

if [ "$1" = 'start' ]; then
  # Validate if DB_HOST is set.
  checkEnv
  # Validate DB Connection
  checkConnection
  # Configure server
  su craft -c "bash -c configureServer"
  # Start server
  su craft -c "node dist/main.js"
fi

exec runuser -u craft "$@"
