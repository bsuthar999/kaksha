import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule, ModalController } from '@ionic/angular';

import { AddDraftPage } from './add-draft.page';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { StorageService } from '../common/services/storage/storage.service';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

describe('AddDraftPage', () => {
  let component: AddDraftPage;
  let fixture: ComponentFixture<AddDraftPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AddDraftPage],
      imports: [HttpClientTestingModule, IonicModule.forRoot()],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [
        {
          provide: ModalController,
          useValue: {},
        },
        {
          provide: StorageService,
          useValue: {
            getItem: (...args) => Promise.resolve('ITEM'),
          },
        },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(AddDraftPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
